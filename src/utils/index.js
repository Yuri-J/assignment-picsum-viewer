import _ from "lodash";

export const constants = {
  api: {
    limit: 24,
    imageTotal: 993,
    imageListURL: "https://picsum.photos/v2/list",
    greyscale: 5,
  },
  debounce: {
    waitMS: 300,
  },
};

const utils = {
  /**
   * Get URL for a single image.
   * @param {string} id Image ID.
   * @param {boolean} greyscale Greyscale mode.
   * @param {boolean} blur Blur mode.
   * @returns Image URL.
   */
  singleImageURL: (id, greyscale, blur) => {
    const isGreyscale =
      _.isBoolean(greyscale) && greyscale ? "grayscale" : undefined;
    const isBlur =
      _.isBoolean(blur) && blur ? `blur=${constants.api.greyscale}` : undefined;
    const params = _.compact([isGreyscale, isBlur]);
    const paramsStr = _.isEmpty(params) ? "" : `?${_.join(params, "&")}`;
    return `https://picsum.photos/id/${id}/510/225${paramsStr}`;
  },
};

export default utils;
