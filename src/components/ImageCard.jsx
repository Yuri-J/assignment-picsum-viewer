import {
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardText,
  MDBCardTitle,
  MDBNavLink,
} from "mdbreact";
import React from "react";

import utils from "utils";

/**
 * Image Card.
 * @param props Image Card Parameters.
 * @returns Image Card Component.
 */
const ImageCard = ({ id, author, width, height, greyscale, blur }) => {
  return (
    <MDBCard className="text-center">
      <MDBNavLink to={`/assignment-picsum-viewer/id/${id}`}>
        <MDBCardImage
          className="img-fluid"
          src={utils.singleImageURL(id, greyscale, blur)}
          waves
        />
      </MDBNavLink>
      <MDBCardBody>
        <MDBCardTitle>{author}</MDBCardTitle>
        <MDBCardText>
          <span className="d-block text-center">
            <span className="text-capitalize font-weight-bold">Picsum ID</span>:{" "}
            {id}
          </span>
          <span className="d-block text-center">
            <span className="text-capitalize font-weight-bold">Dimension</span>:{" "}
            {width} × {height}
          </span>
        </MDBCardText>
      </MDBCardBody>
    </MDBCard>
  );
};

export default ImageCard;
