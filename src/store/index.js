import { configureStore } from '@reduxjs/toolkit';

import imageCardGridReducer, {
  imageCardGridSlice,
  selectImageCardGrid,
} from 'store/imageCardGridSlice';

import picsumReducer, {
  fetchNewImageList,
  picsumSelectors,
  picsumSlice,
  selectPicsum,
} from 'store/picsumSlice';

const store = configureStore({
  reducer: {
    imageCardGrid: imageCardGridReducer,
    picsum: picsumReducer,
  },
});

const { actions: { incrementPageNo } } = picsumSlice;

const { actions: {
  toggleGreyscale,
  toggleBlur,
  setSearchKeyword,
  resetGreyscale,
  resetBlur,
  resetSearchKeyword,
} } = imageCardGridSlice;

export const actions = {
  fetchNewImageList,
  incrementPageNo,
  toggleGreyscale,
  toggleBlur,
  setSearchKeyword,
  resetGreyscale,
  resetBlur,
  resetSearchKeyword,
};

export const selectors = {
  selectImageCardGrid,
  selectPicsum,
};

export const entitySelectors = {
  picsumSelectors,
};

export default store;
