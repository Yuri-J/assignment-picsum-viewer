import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import React from 'react';

import App from 'App';
import store from "store";

describe('App component', () => {
  const renderResult = render(
    <>
      <React.StrictMode>
        <Provider store={store}>
          <App />
        </Provider>
      </React.StrictMode>
    </>
  );

  it('should render brand', () => {
    const brandElement = screen.getByText(/Picsum Viewer/i);
    expect(brandElement).toBeInTheDocument();
  });

  it.skip('should render image card grid row', async () => {
    const gridRowElement = await screen.findByRole('row');
    expect(gridRowElement).toBeInTheDocument();
  });

  it.skip('should render image card grid columns', async () => {
    const gridColElements = await screen.findAllByRole('image-card-grid-col');
    expect(gridColElements).toBeInTheDocument();
  });

  it('should match snapshot', () => {
    const { asFragment } = renderResult;
    expect(asFragment()).toMatchSnapshot();
  });
});
