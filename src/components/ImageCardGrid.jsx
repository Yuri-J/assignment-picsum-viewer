import _ from 'lodash';
import { MDBCol, MDBContainer, MDBRow } from 'mdbreact';
import React, { useEffect } from 'react';

import { useGridDetails } from 'components/ImageCardGrid.hooks';
import ImageCard from 'components/ImageCard';

const ImageCardGrid = () => {
  const { dispatch, fetchNewImageList, pageNo, greyscale, blur, filteredList } = useGridDetails();

  useEffect(() => {
    dispatch(fetchNewImageList(pageNo));
  }, [dispatch, fetchNewImageList, pageNo]);

  return (
    <>
      <MDBContainer className="text-center" style={{ marginTop: 68 }}>
        <MDBRow data-testid="image-card-grid-row">
          {_.map(filteredList, (item, idx) => {
            return (
              <MDBCol key={idx} role="image-card-grid-col" lg="4" md="6" className="justify-content-center my-3">
                <ImageCard {...item} greyscale={greyscale} blur={blur} />
              </MDBCol>
            );
          })}
        </MDBRow>
      </MDBContainer>
    </>
  );
};

export default ImageCardGrid;
