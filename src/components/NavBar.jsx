import { BrowserRouter as Router } from "react-router-dom";
import React from "react";

import {
  MDBCollapse,
  MDBFormInline,
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavbarToggler,
  MDBNavItem,
  MDBNavLink,
} from "mdbreact";

import { useNavBarDetails } from "components/NavBar.hooks";

const NavBar = ({ children }) => {
  const {
    blur,
    greyscale,
    isOpen,
    keyword,
    onChangeSearchKeyword,
    onClickBlur,
    onClickGreyscale,
    onClickNavbarBrand,
    onClickRefresh,
    toggleCollapse,
  } = useNavBarDetails();

  return (
    <Router>
      <MDBNavbar dark color="unique-color" expand="md" fixed="top">
        <MDBNavbarBrand
          href="/assignment-picsum-viewer/"
          onClick={onClickNavbarBrand}
        >
          <strong className="white-text">Picsum Viewer</strong>
        </MDBNavbarBrand>
        <MDBNavbarToggler onClick={toggleCollapse} />
        <MDBCollapse id="navbarCollapse3" isOpen={isOpen} navbar>
          <MDBNavbarNav left>
            <MDBNavItem active={greyscale} onClick={onClickGreyscale}>
              <MDBNavLink to="/assignment-picsum-viewer/">Greyscale</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem active={blur} onClick={onClickBlur}>
              <MDBNavLink to="/assignment-picsum-viewer/">Blur</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem onClick={onClickRefresh}>
              <MDBNavLink to="/assignment-picsum-viewer/">Refresh</MDBNavLink>
            </MDBNavItem>
          </MDBNavbarNav>
          <MDBNavbarNav right>
            <MDBNavItem>
              <MDBFormInline waves>
                <div className="md-form my-0">
                  <input
                    className="form-control mr-sm-2"
                    type="text"
                    placeholder="Search"
                    aria-label="Search"
                    value={keyword}
                    onChange={onChangeSearchKeyword}
                  />
                </div>
              </MDBFormInline>
            </MDBNavItem>
          </MDBNavbarNav>
        </MDBCollapse>
      </MDBNavbar>
      {children}
    </Router>
  );
};

const navBarWithSwitch = (CustomSwitch) => (
  <>
    <NavBar>
      <CustomSwitch />
    </NavBar>
  </>
);

export default navBarWithSwitch;
