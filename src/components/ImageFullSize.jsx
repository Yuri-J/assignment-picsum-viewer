import _ from "lodash";
import { MDBContainer } from "mdbreact";
import { Redirect } from "react-router-dom";
import React from "react";

import { useImageDetails } from "components/ImageFullSize.hooks";

const ImageFullSize = () => {
  const { author, download_url } = useImageDetails();

  return _.isNil(author) || _.isNil(download_url) ? (
    <Redirect to="/" />
  ) : (
    <>
      <MDBContainer className="text-center" style={{ marginTop: 80 }}>
        <h1 className="text-primary">{author}</h1>
        <img alt={author} src={download_url} className="img-fluid d-block" />
      </MDBContainer>
    </>
  );
};

export default ImageFullSize;
