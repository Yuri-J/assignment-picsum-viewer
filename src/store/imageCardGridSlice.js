import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  searchKeyword: '',
  greyscale: false,
  blur: false,
};

export const imageCardGridSlice = createSlice({
  name: 'imageCardGrid',
  initialState,
  reducers: {
    toggleGreyscale: (state) => { state.greyscale = !state.greyscale },
    toggleBlur: (state) => { state.blur = !state.blur },
    setSearchKeyword: (state, action) => { state.searchKeyword = action.payload },
    resetGreyscale: (state) => { state.greyscale = initialState.greyscale },
    resetBlur: (state) => { state.blur = initialState.blur },
    resetSearchKeyword: (state) => { state.searchKeyword = initialState.searchKeyword },
  },
});

export const selectImageCardGrid = state => state.imageCardGrid;

export default imageCardGridSlice.reducer;
