import _ from "lodash";
import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from "@reduxjs/toolkit";
import axios from "axios";

import { constants } from "utils";

// Pick some entity adoptor helps select image details
const picsumAdapter = createEntityAdapter({
  selectId: (picsumImage) => picsumImage.id,
  sortComparer: (a, b) => _.toNumber(a?.id) - _.toNumber(b?.id),
});

export const fetchNewImageList = createAsyncThunk(
  "picsum/fetchNewImageListStatus",
  async (pageNo, _thunkAPI) => {
    const url = `${constants.api.imageListURL}?page=${pageNo}&limit=${constants.api.limit}`;
    const response = await axios.get(url);
    return {
      pageNo,
      status: response.status,
      data: response.data,
    };
  }
);

export const picsumSlice = createSlice({
  name: "picsum",
  initialState: picsumAdapter.getInitialState({
    pageNo: 1,
    changeCounter: 0,
  }),

  reducers: {
    incrementPageNo: (state) => {
      const maxPageNo = _.floor(constants.api.imageTotal / constants.api.limit);
      const pageNoIncremented = state.pageNo + 1;
      state.pageNo = pageNoIncremented <= maxPageNo ? pageNoIncremented : 1;
    },
  },

  // async reducer
  extraReducers: {
    [fetchNewImageList.fulfilled]: (state, action) => {
      const { pageNo, status, data } = action.payload;
      if (status === 200) {
        state.pageNo = pageNo;
        state.changeCounter += 1;
        picsumAdapter.setAll(state, data);
      } else {
        const details = `status: ${status}; data: ${JSON.stringify(data)}`;
        console.error(
          `Unexpected response when fetching new image list; ${details}`
        );
      }
    },
  },
});

export const selectPicsum = (state) => state.picsum;
export const picsumSelectors = picsumAdapter.getSelectors(
  (state) => state.picsum
);

export default picsumSlice.reducer;
