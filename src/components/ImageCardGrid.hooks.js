import _ from 'lodash';
import { createSelector } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';

import store, { actions, entitySelectors, selectors } from 'store';

export const useGridDetails = () => {
  // Redux setup.
  const dispatch = useDispatch();
  const { fetchNewImageList } = actions;
  const { selectImageCardGrid, selectPicsum } = selectors;
  const { picsumSelectors } = entitySelectors;

  // Selectors.
  const selectGreyscale = createSelector([selectImageCardGrid], (imageCardGrid) => imageCardGrid.greyscale);
  const selectBlur = createSelector([selectImageCardGrid], (imageCardGrid) => imageCardGrid.blur);
  const selectSearchKeyword = createSelector([selectImageCardGrid], (imageCardGrid) => imageCardGrid.searchKeyword);
  const selectPageNo = createSelector([selectPicsum], (picsum) => picsum.pageNo);
  const selectChangeCounter = createSelector([selectPicsum], (picsum) => picsum.changeCounter);

  // Selected states.
  const greyscale = useSelector(selectGreyscale);
  const blur = useSelector(selectBlur);
  const searchKeyword = useSelector(selectSearchKeyword);
  const pageNo = useSelector(selectPageNo);

  // Intentionally select change counter for re-rendering.
  useSelector(selectChangeCounter);

  // Selected states from entity adapter.
  const globalStates = store.getState();
  const picsumList = picsumSelectors.selectAll(globalStates);

  // Filtered list.
  const filteredList = _.filter(picsumList, (item) => {
    if (_.isEmpty(searchKeyword)) {
      return true;
    } else {
      const pattern = `${searchKeyword}`;
      const regex = new RegExp(pattern, 'gi');
      const match = _.toString(item?.author).match(regex);
      return !_.isEmpty(match);
    }
  });

  return {
    dispatch,
    fetchNewImageList,
    pageNo,
    greyscale,
    blur,
    filteredList,
  };
};
