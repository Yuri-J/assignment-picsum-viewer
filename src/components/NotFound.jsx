import { MDBContainer, MDBNavLink, MDBBtn } from "mdbreact";
import React from "react";

import styled from "styled-components";
import ErrorImage from "../assets/404.jpg";

const MDBContainerS = styled(MDBContainer)`
  background-color: white;
  padding-top: 9rem;
`;

const NotFound = () => {
  return (
    <>
      <MDBContainerS fluid className="text-center">
        <img src={ErrorImage} height="360" alt="icon" loading="lazy" />
        <h2>Oh No! Page not found.</h2>

        <div className="py-5">
          <MDBNavLink to="/assignment-picsum-viewer">
            <MDBBtn gradient="blue">Go to Home</MDBBtn>
          </MDBNavLink>
        </div>
        <div className="pb-5">
          <a href="https://www.freepik.com/vectors/space">
            Space vector created by stories - www.freepik.com
          </a>
        </div>
      </MDBContainerS>
    </>
  );
};

export default NotFound;
