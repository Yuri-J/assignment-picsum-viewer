import _ from "lodash";
import { createSelector } from "@reduxjs/toolkit";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";

import { actions, selectors } from "store";

import { constants } from "utils";

export const useNavBarDetails = () => {
  // Internal states.
  const [isOpen, setIsOpen] = useState(false);
  const toggleCollapse = () => setIsOpen(!isOpen);
  const [keyword, setKeyword] = useState("");
  const [debouncedSearch, setDebouncedSearch] = useState();
  const [debouncedSearchPending, setDebouncedSearchPending] = useState(false);

  // Redux setup.
  const dispatch = useDispatch();
  const { selectImageCardGrid } = selectors;
  const {
    incrementPageNo,
    toggleGreyscale,
    toggleBlur,
    setSearchKeyword,
    resetGreyscale,
    resetBlur,
    resetSearchKeyword,
  } = actions;

  // Event handlers.
  const onClickNavbarBrand = (e) => {
    dispatch(resetGreyscale());
    dispatch(resetBlur());
    dispatch(resetSearchKeyword());
    setKeyword("");
  };

  const onClickGreyscale = (e) => {
    e.preventDefault();
    dispatch(toggleGreyscale());
  };

  const onClickBlur = (e) => {
    e.preventDefault();
    dispatch(toggleBlur());
  };

  const onClickRefresh = (e) => {
    e.preventDefault();
    dispatch(incrementPageNo());
  };

  const onChangeSearchKeyword = (e) => {
    e.preventDefault();
    const value = e.target.value;
    const debouncee = _.debounce(() => {
      dispatch(setSearchKeyword(value));
      setDebouncedSearchPending(false);
    }, constants.debounce.waitMS);
    // Cancel any pending search.
    if (debouncedSearchPending) {
      debouncedSearch?.cancel();
    }
    // Debounce search.
    debouncee();
    // Update states.
    setKeyword(value);
    setDebouncedSearch(() => debouncee);
    setDebouncedSearchPending(true);
  };

  // Selectors. // stateの位置情報を示す情報
  const selectGreyscale = createSelector(
    [selectImageCardGrid],
    (imageCardGrid) => imageCardGrid.greyscale
  );
  const selectBlur = createSelector(
    [selectImageCardGrid],
    (imageCardGrid) => imageCardGrid.blur
  );

  // Selected states. // dataを位置情報に従い読みだす
  const greyscale = useSelector(selectGreyscale);
  const blur = useSelector(selectBlur);

  return {
    blur,
    greyscale,
    isOpen,
    keyword,
    onChangeSearchKeyword,
    onClickBlur,
    onClickGreyscale,
    onClickNavbarBrand,
    onClickRefresh,
    toggleCollapse,
  };
};
