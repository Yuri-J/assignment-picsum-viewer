import _ from 'lodash';
import { useParams } from 'react-router-dom';

import store, { entitySelectors } from 'store';

export const useImageDetails = () => {
  const params = useParams();
  const id = _.get(params, 'id');

  // Redux setup.
  const { picsumSelectors } = entitySelectors;

  // Selected states from entity adapter.
  const globalStates = store.getState();
  const imageDetails = picsumSelectors.selectById(globalStates, id);

  return {
    author: imageDetails?.author,
    download_url: imageDetails?.download_url,
  };
};
