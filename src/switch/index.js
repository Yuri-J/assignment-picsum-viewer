import { Redirect, Route, Switch } from "react-router-dom";

import ImageCardGrid from "components/ImageCardGrid";
import ImageFullSize from "components/ImageFullSize";
import NotFound from "components/NotFound";

const CustomSwitch = () => (
  <>
    <Switch>
      <Route exact path={["/assignment-picsum-viewer/"]}>
        <ImageCardGrid />
      </Route>
      <Route path="/assignment-picsum-viewer/id/:id">
        <ImageFullSize />
      </Route>
      <Route exact path="/assignment-picsum-viewer/notfound">
        <NotFound />
      </Route>
      <Route path="/">
        <Redirect to="/assignment-picsum-viewer/notfound" />
      </Route>
    </Switch>
  </>
);

export default CustomSwitch;
