# Picsum Viewer

#### Assessment 2, Term 1, 2021

## Launch App

Run the following in the command line:

```bash
yarn start
```

## Features

Upon launching, Picsum Viewer automatically loads 24 pictures from the [Lorem Picsum](http://picsum.photo/) API.

### Greyscale

Click the `Greyscale` button in the menu bar. All pictures will be shown in the greyscale mode.

### Blur

Click the `Blur` button in the menu bar. All pictures will be shown in the blurry mode.

### Refresh

Click the `Refresh` button in the menu bar. The image grid will load a new set of pictures.

### Search

Input keywords in the `Search` input box in the menu bar. Picsum Viewer will filter pictures based on the keyword match.

### Full Size Picture

Click on any picture in the grid, a full sized image will be shown with the author name as its title.

### Not Found Page

When an unrecognised path is given, Picsum Viewer will present a Not Found page.
